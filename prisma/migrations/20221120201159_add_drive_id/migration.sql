/*
  Warnings:

  - A unique constraint covering the columns `[drive_folder_uid]` on the table `Playlist` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `drive_folder_uid` to the `Playlist` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Playlist" ADD COLUMN     "drive_folder_uid" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "Playlist_drive_folder_uid_key" ON "Playlist"("drive_folder_uid");
