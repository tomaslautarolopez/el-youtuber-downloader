-- CreateTable
CREATE TABLE "Playlist" (
    "id" SERIAL NOT NULL,
    "youtube_uid" TEXT NOT NULL,

    CONSTRAINT "Playlist_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Song" (
    "id" SERIAL NOT NULL,
    "youtube_uid" TEXT NOT NULL,
    "playlist_id" INTEGER NOT NULL,
    "youtube_url" TEXT NOT NULL,

    CONSTRAINT "Song_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Playlist_youtube_uid_key" ON "Playlist"("youtube_uid");

-- CreateIndex
CREATE UNIQUE INDEX "Song_youtube_uid_key" ON "Song"("youtube_uid");

-- AddForeignKey
ALTER TABLE "Song" ADD CONSTRAINT "Song_playlist_id_fkey" FOREIGN KEY ("playlist_id") REFERENCES "Playlist"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
