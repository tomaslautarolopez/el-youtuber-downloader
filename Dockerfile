FROM node:16.14.2 as build

WORKDIR /usr/src/application
ADD ./ /usr/src/application

RUN npx prisma generate; yarn build && npm prune --production

FROM node:16.14.2-alpine

COPY --from=build /usr/src/application /usr/src/application
WORKDIR /usr/src/application

EXPOSE 3000

CMD node dist/index.js
