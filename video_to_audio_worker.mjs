import workerpool from "workerpool"
import ffmpeg from "fluent-ffmpeg";
import { path } from "@ffmpeg-installer/ffmpeg";

ffmpeg.setFfmpegPath(path);

async function videoToAudioWorker(videoInputPath, audioOutputPath) {
    return new Promise((resolve, reject) => {
        ffmpeg(videoInputPath)
            .audioBitrate(128)
            .save(audioOutputPath)
            .on("error", function (err) {
                console.log(err)
                reject(err)

            })
            .on("end", () => {
                resolve(undefined);
            });
    });

}

// create a worker and register public functions
workerpool.worker({
    videoToAudioWorker
});