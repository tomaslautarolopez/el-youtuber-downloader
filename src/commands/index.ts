
import fs from "fs"
import path from "path";
import { Google } from "../adapters/google";
import { Postgres } from "../adapters/postgres";
import { ElYoutubeDownloader } from "../adapters/youtube";

class Commands {
    elYoutubeDownloader: ElYoutubeDownloader

    constructor() {
        this.elYoutubeDownloader = new ElYoutubeDownloader(
            `${__dirname}/../../temp`,
            `${__dirname}/../../canciones`
        );
    }

    fetchPlaylists() {
        fs.readFile("./playlist.txt", async (err, data) => {

            if (err) throw err;

            const playlists = data.toString().split("\n");

            await Promise.all(playlists.map(this.elYoutubeDownloader.downloadPlaylistMp3));
        });

    }

    fetchSongs() {
        fs.readFile("./links.txt", async (err, data) => {
            if (err) throw err;

            const links = data.toString().split("\n");

            await Promise.all(links.map(this.elYoutubeDownloader.downloadMp3));
        });
    }

    async synchPlaylistsDb() {
        const postgres = new Postgres();
        const google = new Google();

        console.log("Starting");
    
        try {
            const playlists = await postgres.getAllPLaylists();

            await Promise.all(playlists.map(async (playlist) => {
                const playlistData = await this.elYoutubeDownloader.getPlaylist(playlist.youtube_uid, {
                    limit: Infinity
                });

                const songsToSave = await postgres.checkNotCreatedSongs(playlistData.items);

                await Promise.all(songsToSave.map(async (song) => {
                    try {
                        const ys = await this.elYoutubeDownloader.downloadMp3(song.url);
                        const songPath = path.join(__dirname, `../../canciones/${ys.videoDetails.title}.mp3`)

                        await google.uploadFile({
                            mimeType: "audio/mp3",
                            name: ys.videoDetails.title,
                            folder: playlist.drive_folder_uid,
                            fileBuffer: fs.createReadStream(songPath)
                        })

                        fs.promises.unlink(songPath)
                        await postgres.insertSong(ys, playlist)
                    } catch (error) {
                        console.log("Failed song ", song.url)
                        console.log(error)
                    }
                }))

            }))

        } catch (error) {
            console.log(error)
        }

        await postgres.close()
        console.log("Finished")
    }

}

export const Commander = new Commands()