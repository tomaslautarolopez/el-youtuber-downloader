import { drive_v3, google } from 'googleapis';
import { GoogleAuth } from 'google-auth-library';
import { JSONClient } from 'google-auth-library/build/src/auth/googleauth';
import path from 'path';
import { ReadStream } from 'fs';


export class Google {
    private auth: GoogleAuth<JSONClient>
    private KEY_FILE_PATH = path.join(__dirname, '../../../config/service.json');
    private SCOPES = ['https://www.googleapis.com/auth/drive'];
    private MAIN_FOLDER = "1dPod7KLfIxx2fidNf5meai3WX0BtnyDX";
    drive: drive_v3.Drive;

    constructor() {
        this.auth = new GoogleAuth({
            keyFile: this.KEY_FILE_PATH,
            scopes: this.SCOPES,
        });

        this.drive = google.drive({ version: 'v3', auth: this.auth });
    }

    async createFolder(name: string, fields = 'id') {
        const { data: folder } = await this.drive.files.create({
            resource: {
                mimeType: 'application/vnd.google-apps.folder',
                name: name,
                parents: [this.MAIN_FOLDER],
            },
            fields,
        } as any);

        return folder
    }

    async uploadFile(file: {
        fileBuffer: ReadStream,
        mimeType: string,
        name: string,
        folder: string
    }, fields = 'id,name') {
        const { data: uploadedFile } = await this.drive.files.create({
            media: {
                mimeType: file.mimeType,
                body: file.fileBuffer,
            },
            requestBody: {
                name: file.name,
                parents: [file.folder],
            },
            fields,
        });

        return uploadedFile
    };
}