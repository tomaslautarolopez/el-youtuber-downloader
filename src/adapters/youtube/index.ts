import ytdl from "ytdl-core";
import ytpl from "ytpl";
import fs from "fs";
import ffmpeg from "fluent-ffmpeg";
import { path } from "@ffmpeg-installer/ffmpeg";
import workerpool from "workerpool";
import sanitize from "sanitize-filename"

import { RateLimiter } from "../../utils/rate_limiter";

const pool = workerpool.pool(__dirname + "/video_to_audio_worker.mjs", {
  minWorkers: 4,
});

ffmpeg.setFfmpegPath(path);

export class ElYoutubeDownloader extends RateLimiter {
  constructor(
    private readonly videoPath: string,
    private readonly songPath: string
  ) {
    super(10, 1000);

    this.downloadVideo = this.rateLimiter(this.downloadVideo);
  }

  async downloadVideo(link: string, path: string) {
    const video = ytdl(link, { quality: "highestaudio" });
    const info = await ytdl.getInfo(link);
    const title = sanitize(info.videoDetails.title)

    info.videoDetails.title = title;

    const videoOutputFile = `${path}/${title}.mp4`;
    const videoStream = video.pipe(fs.createWriteStream(videoOutputFile));

    return new Promise((resolve: (value: ytdl.videoInfo) => void, reject) => {
      videoStream
        .on("finish", async () => {
          console.log("Video was donwloaded to ", videoOutputFile);
          resolve(info);
        })
        .on("error", reject);
    });
  }

  async covertVideoWithWorker(pathInput: string, pathOutput: string) {
    console.log("Started to convert ", pathInput);
    await pool.exec("videoToAudioWorker", [pathInput, pathOutput]);
    console.log("Converted video to mp3 ", pathOutput);
  }

  covertVideoToMp3(pathInput: string, pathOutput: string) {
    console.log("Started to convert ", pathInput);

    return new Promise((resolve, reject) => {
      ffmpeg(pathInput)
        .audioBitrate(128)
        .save(pathOutput)
        .on("error", function (err) {
          console.log(err);
          reject(err);
        })
        .on("end", () => {
          console.log("Converted video to mp3 ", pathOutput);
          resolve(undefined);
        });
    });
  }

  async downloadMp3(url: string) {
    const info = await this.downloadVideo(url, this.videoPath);
    const title = info.videoDetails.title

    await this.covertVideoToMp3(
      `${this.videoPath}/${title}.mp4`,
      `${this.songPath}/${title}.mp3`
    );
    await fs.promises.unlink(`${this.videoPath}/${title}.mp4`);

    return info;
  }

  async getPlaylist(playlistId: string, options?: ytpl.Options) {
    return await ytpl(playlistId, options);
  }

  async downloadListOfSongMp3(songs: ytpl.Item[]): Promise<ytdl.videoInfo[]> {
    const downloadedSongs: ytdl.videoInfo[] = [];

    await Promise.all(songs.map(async (item) => {
      try {
        const song = await this.downloadMp3(item.url);

        downloadedSongs.push(song);
      } catch (error) {
        console.log(error);
      }
    }));

    return downloadedSongs;
  }

  async downloadPlaylistMp3(playlistId: string) {
    try {
      const playlist = await ytpl(playlistId);

      await this.downloadListOfSongMp3(playlist.items)
    } catch (error) {
      console.log("This playlist explode ", playlistId);
      console.log(error);
    }
  }
}
