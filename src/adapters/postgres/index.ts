import { Playlist, PrismaClient } from "@prisma/client";
import ytdl from "ytdl-core";
import ytpl from "ytpl";

export class Postgres {
    private readonly PrismaClient: PrismaClient = new PrismaClient();
    private static instance: Postgres

    constructor() {

        if (Postgres.instance) {
            return Postgres.instance
        } else {
            Postgres.instance = this;

            return Postgres.instance
        }

    }

    async getAllPLaylists() {
        return this.PrismaClient.playlist.findMany({})
    }

    async insertSong(song: ytdl.videoInfo, playlist: Playlist) {
        return this.PrismaClient.song.create({
            data: {
                youtube_uid: song.videoDetails.videoId,
                playlist_id: playlist.id,
                youtube_url: song.videoDetails.video_url,
            }
        })
    }

    async checkNotCreatedSongs(songs: ytpl.Item[]) {
        const presentSongs = await this.PrismaClient.song.findMany({
            where: {
                youtube_uid: {
                    in: songs.map((song) => song.id)
                }
            }
        })

        return songs.filter((song) => !presentSongs.find((dsong) => dsong.youtube_uid === song.id))
    }

    close() {
        return this.PrismaClient.$disconnect()
    }
}