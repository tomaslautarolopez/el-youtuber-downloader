import { CronJob } from "cron"
import { Commander } from "./commands";

export const job = new CronJob(
	'30 * * * *',
	async () => {
		await Commander.synchPlaylistsDb()
	},
	null,
	true,
	'America/Argentina/Buenos_Aires',
	null,
	true
);