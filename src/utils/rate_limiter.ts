export class RateLimiter {
    functionsCalls: Record<
        string,
        {
            numberOfCalls: number;
            stackedCalls: {
                resolve: (value: any) => void;
                reject: (reason?: any) => void;
                args: any;
            }[];
        }
    > = {};

    constructor(
        private readonly batchSize: number,
        private readonly throttleMs: number
    ) { }

    increaseCalls(functionName: string) {
        const functionsCall = this.functionsCalls[functionName];

        if (functionsCall) {
            this.functionsCalls[functionName] = {
                numberOfCalls: functionsCall.numberOfCalls + 1,
                stackedCalls: functionsCall.stackedCalls,
            };
        }
    }

    rateLimiter<Args extends any[], Ret>(
        fun: (...args: Args) => Promise<Ret>
    ): (...args: Args) => Promise<Ret> {
        const functionName = fun.name;

        this.functionsCalls[functionName] = {
            numberOfCalls: 0,
            stackedCalls: [],
        };

        setInterval(() => {
            const { stackedCalls } = this.functionsCalls[functionName];

            const callsToMake = stackedCalls.slice(0, this.batchSize);
            const leftoverCalls = stackedCalls.slice(
                this.batchSize,
                stackedCalls.length
            );

            callsToMake.forEach(async ({ args, resolve, reject }) => {
                try {
                    resolve(await fun(...args));
                } catch (error) {
                    reject(error);
                }
            });

            this.functionsCalls[functionName] = {
                numberOfCalls: callsToMake.length < this.batchSize ? this.batchSize : 0,
                stackedCalls: leftoverCalls,
            };
        }, this.throttleMs);

        return (...args: Args) => {
            const { numberOfCalls, stackedCalls } = this.functionsCalls[functionName];

            if (numberOfCalls <= this.batchSize) {
                this.increaseCalls(functionName);

                return fun(...args);
            } else {
                return new Promise((resolve, reject) => {
                    stackedCalls.push({
                        resolve,
                        reject,
                        args,
                    });
                });
            }
        };
    }
}
